#!/usr/bin/env perl

use strict;
use warnings;

my @month_name = (
  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
);

my $month_hash_ref = {
  Jan => 1, Feb => 2, Mar => 3, Apr => 4, May => 5, Jun => 6, Jul => 7, Aug => 8, Sep => 9, Oct => 10, Nov => 11, Dec => 12,
};

# foreach my $name (sort keys %{$month_hash_ref}){
for my $name (@month_name){
  print $name . ' is ' . ${$month_hash_ref}{$name};
  if ($name eq "Jan") {
    print "st";
  } elsif($name eq "Feb") {
    print "nd";
  } elsif($name eq "Mar") {
    print "rd";
  } else {
    print "th";
  }
  print " month\n";
}
