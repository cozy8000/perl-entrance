#!/usr/bin/env perl

use strict;
use warnings;

my %animal = (
  dog => {name => 'Taro', color => 'brown'},
  cat => {name => 'Tama', color => 'white'},
);

print $animal{cat}{color} . "\n";

foreach my $type (keys %animal) {
  print $type . "\n";
  for my $property (keys %{$animal{$type}}) {
    print $property . " : " . $animal{$type}->{$property} . "\n";
  }
}
