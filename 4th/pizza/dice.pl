#!/usr/bin/env perl

# すごろく
#
# 0から10までのマスがあり、0からスタートし、10に行けばゴールするすごろくがあります。
# 進む距離は1〜6の細工されていないサイコロで決めます。
# ただし、3、6、9のマスに止まった場合、スタートの0に戻されます。
# さて、平均的にサイコロを何回振ればゴールできるでしょうか？

use strict;
use warnings;

#
# Configuration
#

# サンプルの数(10 ** MAX_NUMBER_OF_POWERS)
use constant MAX_NUMBER_OF_POWERS => 7;

#
# Subroutine
#

sub roll_dice {

  my $trial_count = shift;
  my $sum = 0;

  for my $i (1 .. $trial_count) {
    # 試行回数用の変数
    my $trial = 0;
    # スタート地点をセット
    my $level = 0;
    while (1) {
      # サイコロを振る
      $trial++;
      # サイコロの目
      $level += int(rand(6)) + 1;
      # levelが3,6,9だったらスタート地点に移動
      $level = 0 unless $level % 3;
      if ($level >= 10) {
        # サイコロを振った回数を足す
        $sum += $trial;
        last;
      }
    }
  }
  # 期待値を計算してreturn
  return $sum / $trial_count;
}

#
# Main
#

for (my $power = 0; $power <= MAX_NUMBER_OF_POWERS; $power++) {

  # サンプルの数
  my $number_of_sample = 10 ** $power;

  # $number_of_sample回、すごろくを実行し期待値を算出
  my $expected_value = roll_dice($number_of_sample);

  # 結果表示
  printf "サイコロを試行した回数: %8d / 期待値: %7.4f\n" , $number_of_sample , $expected_value;

}

#
# Output (ex.)
#

# サイコロを試行した回数:        1 / 期待値:  3.0000
# サイコロを試行した回数:       10 / 期待値:  6.2000
# サイコロを試行した回数:      100 / 期待値:  7.4600
# サイコロを試行した回数:     1000 / 期待値:  7.7220
# サイコロを試行した回数:    10000 / 期待値:  7.3647
# サイコロを試行した回数:   100000 / 期待値:  7.3718
# サイコロを試行した回数:  1000000 / 期待値:  7.3798
# サイコロを試行した回数: 10000000 / 期待値:  7.3781
