#!/usr/bin/env perl

use strict;
use warnings;
use Data::Dumper;

# japan
#  ├─ tokyo
#  │    ├─ gotanda
#  │    └─ shibuya
#  ├─ osaka
#  │    └─ shinsaibashi
#  ├─ okinawa
#  │    ├─ naha
#  │    └─ yomitan
#  ├─ hokkaido
#  │    ├─ sapporo
#  │    └─ obihiro
#  └─ fukuoka
#        └─ hakata

my %japan = (
  tokyo => ['gotanda', 'shibuya'],
  osaka => ['shinsaibashi'],
  okinawa => ['naha', 'yomitan'],
  hokkaido => ['sapporo', 'obihiro'],
  fukuoka => ['hakata'],
);

# 作成したデータから tokyo に含まれる gotanda, shibuyaを表示してください
for my $city (@{$japan{tokyo}}) {
  print $city . "\n";
}

# 作成したデータの osaka に umedaを追加してください
push @{$japan{osaka}}, 'umeda';

# 作成したデータの okinawa に piyoを追加してください
push @{$japan{okinawa}}, 'piyo';

# 作成したデータの hokkaido に chitoseを追加し, sapporoを削除してください
print Dumper \%japan;
push @{$japan{hokkaido}}, 'chitose';
print Dumper \%japan;

# shift @{$japan{hokkaido}};

my $pos;
my $i = 0;
for my $city (@{$japan{hokkaido}}) {
  $i++;
  if ($city eq 'sapporo') {
    $pos = $i;
  }
}

print "$pos" . "===\n";
@{$japan{hokkaido}} = splice @{$japan{hokkaido}}, $pos, 1;
print Dumper \%japan;

# 作成したデータの fukuoka を削除してください
delete $japan{fukuoka};

print Dumper \%japan;
