#!/usr/bin/env perl

use strict;
use warnings;

print 'Input first number> ';
my $first = <STDIN>;
chomp $first;

print 'Input second number> ';
my $second = <STDIN>;
chomp $second;

print "$first + $second = " . ($first + $second) . "\n";
print "$first - $second = " . ($first - $second) . "\n";
print "$first * $second = " . ($first * $second) . "\n";
print "$first / $second = " . ($first / $second) . "\n";
