#!/usr/bin/env perl

# guess-ad-year.pl - 和暦を引数に与えると西暦年を応えてくれる
# ad は A.D. (Anno Domini = 西暦年) の略
#
# 例：
# guess-ad-year.pl 平成30年
# 西暦2018年
# guess-ad-year.pl 昭和54年
# 西暦1979年

# guess-ad-year.{pl,rb} は perl guess-ad-year.pl 平成30年 とコマンドを打つと 西暦2018年 と表示される、和暦→西暦変換プログラムです
# guess-ad-year.{pl,rb} で以下の部分を対応してみましょう

#     令和対応 をして下さい。 perl guess-ad-year.pl 令和2年 と打つと 西暦2020年 と返すようにして下さい
#     元年表記対応 をして下さい。このプログラムは 平成1年 という入力は受け付けますが 平成元年 という入力を受け付けることができません。これを受け付けるようにして下さい
#     明治・大正対応 をして下さい

use strict;
use warnings;
use utf8;                   # ソースコードでマルチバイト文字を使う際に利用
use Encode 'decode';

binmode(STDIN, ':utf8');    # 標準入力をutf8として認識する
binmode(STDOUT, ':utf8');   # 標準出力をutf8にする
binmode(STDERR, ':utf8');   # 標準エラー出力をutf8にする

my %JAPANESE_ERA = (
    '令和' => 2018,
    '平成' => 1988,
    '昭和' => 1925,
    '大正' => 1911,
    '明治' => 1867,
);

my $jp_year = shift || "";  # 後述の正規表現マッチで穏当にマッチを外すよう、未定義だった場合は空文字を設定する
$jp_year = decode('UTF-8', $jp_year);
$jp_year =~ tr/０-９/0-9/;  # 全角数字を半角数字に変換
$jp_year =~ s/元年/1年/;  # 元年を1年に変換

if (my ($jp_era_name, $jp_era_year) = $jp_year =~ /^(.*?)(\d+)年$/) {

    # %JAPANESE_ERAに元号があるか確認
    die "認識できない元号です\n" unless exists $JAPANESE_ERA{$jp_era_name};;

    # %JAPANESE_ERAからbase_ad_yearを抽出
    my $base_ad_year = $JAPANESE_ERA{$jp_era_name};

    # 西暦を計算
    my $ad_year = $base_ad_year + $jp_era_year;
    print "西暦${ad_year}年\n";

} else {
    die "認識できない書式です\n";
}
