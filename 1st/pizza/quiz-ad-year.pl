#!/usr/bin/env perl

# guess-ad-year.pl のQuiz版
#     「平成？？年は西暦何年でしょう」
#     「令和？？年は西暦何年でしょう」
#
#      (令和、平成、昭和、大正、明治)
#      問題の範囲は 1867 - 2019年

use strict;
use warnings;
use utf8;                   # ソースコードでマルチバイト文字を使う際に利用
use Encode 'decode';

binmode(STDIN, ':utf8');    # 標準入力をutf8として認識する
binmode(STDOUT, ':utf8');   # 標準出力をutf8にする
binmode(STDERR, ':utf8');   # 標準エラー出力をutf8にする

my $START_YEAR = 1867;
my $END_YEAR = 2019;

my $JAPANESE_ERA = [
  {'令和' => 2018},
  {'平成' => 1988},
  {'昭和' => 1925},
  {'大正' => 1911},
  {'明治' => 1867},
];

# 問題の西暦(答え)をrandomに生成
my $year = int(rand() * ($END_YEAR - $START_YEAR + 1) + $START_YEAR);

my $quiz_era = "";
my $quiz_num = 0;

# 西暦(答え)から元号と年を算出
for my $era (@$JAPANESE_ERA) {
  my $finish;
  for my $key (keys %$era) {
    if (int($era->{$key}) < $year) {
      $quiz_era = $key;
      $quiz_num = $year - $era->{$key};
      $finish = 1;
    }
  }
  last if $finish;
}

# 問題の表示
print "${quiz_era}${quiz_num}年 は 西暦何年 でしょう? > ";
my $answer = <STDIN>;
chomp $answer;
$answer =~ tr/０-９/0-9/;  # 全角数字を半角数字に変換

# 答え合わせ、結果を表示
print "\n";
if ($answer == $year) {
        print "正解です!\n";
} else {
        print "残念、間違いです!\n";
}
print "${quiz_era}${quiz_num}年 は 西暦${year}年 です。\n";
