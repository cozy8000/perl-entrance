#!/usr/bin/env perl

# https://github.com/perl-entrance-org/workshop-2019/blob/master/1st/practice.md
# 文字列の連結 #1

use strict;
use warnings;

my $foo = 'P';
my $bar = 'e';
my $buz = 'r';
my $hoge = 'l';

print "${foo}${bar}${buz}${hoge}\n";
