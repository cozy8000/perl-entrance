#!/usr/bin/env perl

# https://github.com/perl-entrance-org/workshop-2019/blob/master/1st/practice.md
# 標準入力 #2

use strict;
use warnings;

my $str = <STDIN>;
chomp $str;

print "$str$str\n";

