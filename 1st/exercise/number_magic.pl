#!/usr/bin/env perl

# https://github.com/perl-entrance-org/workshop-2019/blob/master/1st/practice.md
# 四則演算 #1

use strict;
use warnings;

my $foo = 10;
my $hoge = $foo;

$hoge += 1;
$hoge *= 2;
$hoge += 6;
$hoge /= 2;
$hoge -= $foo;

print "$hoge\n";
