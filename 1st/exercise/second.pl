#!/usr/bin/env perl

# https://github.com/perl-entrance-org/workshop-2019/blob/master/1st/practice.md
# 四則演算 #2 1.

use strict;
use warnings;

my $min = int(150 / 60);
my $sec = 150 % 60;

print "$min\n";
print "$sec\n";
