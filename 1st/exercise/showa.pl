#!/usr/bin/env perl

# https://github.com/perl-entrance-org/workshop-2019/blob/master/1st/practice.md
# 文字列の連結 #2

use strict;
use warnings;

my $last_taisho = 1925;

print "西暦2019年は昭和" . (2019 - $last_taisho) . "年です\n";
