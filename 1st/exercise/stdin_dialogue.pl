#!/usr/bin/env perl

# https://github.com/perl-entrance-org/workshop-2019/blob/master/1st/practice.md
# 標準入力 #3

use strict;
use warnings;

print "Please tell Your Name ? > ";
my $name = <STDIN>;
chomp $name;

print "What time is it now ? > ";
my $time = <STDIN>;
chomp $time;

print "You are $name.\n";
print "It is $time o'clock.\n";
