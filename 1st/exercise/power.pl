#!/usr/bin/env perl

# https://github.com/perl-entrance-org/workshop-2019/blob/master/1st/practice.md
# 四則演算 #2 2.

use strict;
use warnings;

my $answer = 10 ** 2 * 10 ** 3 - 10 ** 5;

print "$answer\n";
