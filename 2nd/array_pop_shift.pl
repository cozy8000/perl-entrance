#!/usr/bin/env perl

use strict;
use warnings;

my @array = ('Alice', 'Bob', 'Chris');
my $string = pop @array;
print "$string\n";

$string = shift @array;
print "$string\n";
