#!/usr/bin/env perl

use strict;
use warnings;

my $answer = 10;

print 'input the number> ';
my $input = <STDIN>;
chomp $input;

if ($answer == $input) {
  print "OK\n";
} elsif ($answer - 5 <= $input && $input <= $answer + 5) {
  print "near\n";
} elsif ($answer < $input) {
  print "too big\n";
} elsif ($answer > $input) {
  print "too small\n";
}
