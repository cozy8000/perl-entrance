#!/usr/bin/env perl

use strict;
use warnings;

my %my_profile = (
  name => 'kobayashi',
  age => '40+',
  food => 'apple',
);

my @keys = keys %my_profile;
print "@keys\n";

delete $my_profile{age};

for my $key (keys %my_profile) {
  my $value = $my_profile{$key};
  print "$key => $value\n";
}

my @keys2 = qw(name age food);

foreach my $key (@keys2) {
  if (exists $my_profile{$key}) {
    print "$key is exist.\n";
  } else {
    print "$key is not exist.\n";
  }
}
