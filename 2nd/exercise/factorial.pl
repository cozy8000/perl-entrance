#!/usr/bin/env perl

use strict;
use warnings;

print 'input the number> ';
my $input = <STDIN>;
chomp $input;

my $result = 1;

for (1 .. $input) {
  $result *= $_;
}

print "$result\n";
