#!/usr/bin/env perl

use strict;
use warnings;

print 'input the number > ';
my $input = <STDIN>;
chomp $input;

if ($input % 2 == 0 && $input % 3 == 0) {
  print "Multiple of six\n";
}
