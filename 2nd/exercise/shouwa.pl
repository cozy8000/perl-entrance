#!/usr/bin/env perl

use strict;
use warnings;

my $seireki = 2019;
my $last_taisho = 1925;
my $result = $seireki - $last_taisho;

print "西暦" . $seireki . "年は昭和" . $result . "年です\n";
