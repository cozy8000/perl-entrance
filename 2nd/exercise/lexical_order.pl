#!/usr/bin/env perl

use strict;
use warnings;

print 'input the first string> ';
my $input1 = <STDIN>;
chomp $input1;

print 'input the second string> ';
my $input2 = <STDIN>;
chomp $input2;

if ($input1 eq $input2) {
  print "even\n";
} elsif ($input1 gt $input2) {
  print "$input2 $input1\n";
} else {
  print "$input1 $input2\n";
}
