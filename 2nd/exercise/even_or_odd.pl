#!/usr/bin/env perl

use strict;
use warnings;

print 'input the number> ';
my $input = <STDIN>;
chomp $input;

if ($input % 2) {
  print "odd\n";
} else {
  print "even\n";
}
