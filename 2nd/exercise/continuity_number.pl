#!/usr/bin/env perl

use strict;
use warnings;

print 'input the number> ';
my $input = <STDIN>;
chomp $input;
my @array = (0 .. $input);

for (@array) {
  print "$_\n";
}
