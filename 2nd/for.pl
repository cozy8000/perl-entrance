#!/usr/bin/env perl

use strict;
use warnings;

my @array = ("aa", "bb", "cc");

foreach my $string (@array) {
    print "$string\n";
}

my @array2 = <STDIN>;

foreach my $string (@array2) {
  chomp $string;
  print "[$string]\n";
}
