#!/usr/bin/env perl

use strict;
use warnings;

my @array = ('Alice', 'Bob', 'Chris');
push @array, 'Diana';
unshift @array, 'Eve';

foreach my $string (@array) {
  print "$string\n";
}
