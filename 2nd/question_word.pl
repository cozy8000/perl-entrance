#!/usr/bin/env perl

use strict;
use warnings;

my $answer = 'perl';

print 'input secret word> ';
my $input = <STDIN>;
chomp $input;

if ($answer eq $input) {
  print "OK\n";
} else {
  print "NG\n";
}
