#!/usr/bin/env perl

# 正弦波 y = sin(x) を書く
#
# 参考例
#
# 0.05秒に1回プロットする
# x は 0 から pi/10 ずつ増えていく
# sin(x) は -1 から 1 までの値を取るので、10倍して +10 すると 0 から 20 までの値となる。これの整数部分の個数 "*" を描く
# int( 10 * sin ( $x ) + 10 )
# Perl のヒント
#
# sin 組み込み関数あり
# ただし引数はラジアン
# use Math::Trig qw(pi) すれば円周率 pi が使える
# use Time::HiRes qw(sleep) とやると1秒未満の停止もできる賢い sleep が使える（sleep 0.05; とか書ける）

use strict;
use warnings;

use Math::Trig qw(pi);
use Time::HiRes qw(sleep);

use constant SLEEP_TIME => 0.05;

# *の数
my $i;

while (1) {
  for (my $x = 0; $x <= pi * 2; $x += pi / 10) {
    $i =  int((sin($x) + 1) * 10);
    print "*" x $i . "\n";
    sleep(SLEEP_TIME);
  }
}
