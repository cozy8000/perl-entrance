#!/usr/bin/env perl

use strict;
use warnings;

my @word = ('ズン', 'ドコ');
my $target;
my $random;

while (1) {
  # 0..1 random生成
  $random = int(rand(@word));
  print "$word[$random]\n";

  # $targetに追加
  $target .= $word[$random];
  if ($word[$random] eq 'ドコ') {
    # 評価
    if ($target eq 'ズンズンズンズンドコ') {
      last; # 成功
    } else {
      $target = ''; # 失敗
    }
  }
}

print "キ・ヨ・シ!\n";
