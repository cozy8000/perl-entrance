#!/usr/bin/env perl

use strict;
use warnings;

my $list = "There's more than one way to do it.";
my @array = split / /, $list;
foreach my $i (@array) {
  print "$i\n";
}

$list = "There's more than one way to do it.";
my @array2 = split ' m', $list;
foreach my $i (@array2) {
  print "$i\n";
}
