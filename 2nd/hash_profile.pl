#!/usr/bin/env perl

use strict;
use warnings;

my %my_profile = (
  name => 'kobayashi',
  age => '40+',
  food => 'apple',
);

print "$my_profile{name}\n";
print "$my_profile{age}\n";
print "$my_profile{food}\n";
