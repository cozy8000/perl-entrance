#!/usr/bin/env perl

use strict;
use warnings;

print 'input the string > ';
my $input = <STDIN>;
chomp $input;

perl_check($input);

sub perl_check {
  my $input = shift;

  if ($input =~ /[Pp]erl/) {
    print "Perl Monger!";
  }
}
