#!/usr/bin/env perl

use strict;
use warnings;

my %data = (
  alice => 'sushi',
  bob   => 'soba',
  carol => 'sushi',
  dave  => 'sushi',
  ellen => 'soba',
  frank => 'udon',
);

my %count = ();

for my $key (keys %data) {
  $count{$data{$key}}++;
}

for my $key (keys %count) {
  print "$key => $count{$key}\n";
}
