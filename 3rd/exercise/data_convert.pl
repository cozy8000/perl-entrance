#!/usr/bin/env perl

use strict;
use warnings;

# 「2019-07-13」の形式に直す。

my $date = '07-13-2019';

$date =~ s/(\d+)-(\d+)-(\d+)/$3-$1-$2/;

print $date . "\n";
