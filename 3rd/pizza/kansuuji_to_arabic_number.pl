#!/usr/bin/env perl

use strict;
use warnings;

use utf8;
use Encode qw 'encode';

for my $line (<DATA>) {
  chomp($line);
  print "[before] : " . encode('UTF-8', $line) . "\n";
  if ($line =~ '一十') {
    print "NG 1\n";
    next;
  }
  if ($line =~ /[一二三四五六七八九〇]{2}/) {
    print "NG 2\n";
    next;
  }

  $line =~ s/^十$/10/;
  $line =~ s/^十/1/;
  $line =~ s/十$/0/;
  $line =~ s/十//;
  $line =~ tr/一二三四五六七八九〇/1234567890/;
  print "[after ] : " . encode('UTF-8', $line) . "\n";
}
    # 十 が先頭 かつ その後に何の文字もない場合は 10 に置換
    # $arabic_number =~ s/^十$/10/;
    #
    # # 肯定的先読み(?=pattern)
    # # 先頭に 十 があり、十 の後に文字が続く場合に 十を置換
    # $arabic_number =~ s/(^十)(?=.+$)/1/;
    #
    # # 十 が他のアラビア数字に挟まれている場合、 十 を削除
    # # 肯定的後読み(?<=pattern)
    # # 肯定的先読み(?=pattern)
    # $arabic_number =~ s/(?<=\d)十(?=\d)//;
#   # my $arabic_number_count = () = $check_number =~ /＋/g;
#   # $arabic_number_count = () = 2;

__DATA__
〇
一
二
十
十一
二十一
三十四
三〇
三十
十十
一十
一一
十〇
〇十
